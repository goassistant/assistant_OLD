###################################
#
# TODO LIST
#
# - sudo sysctl -w vm.max_map_count=262144 # for elasticsearch in docker
#   OR :
#   	root@ambre:/media/data/gobutler/assistant/elk# cat /proc/sys/vm/max_map_count
#	65530
#	root@ambre:/media/data/gobutler/assistant/elk# ls -l /proc/sys/vm/max_map_count
#	-rw-r--r-- 1 root root 0 déc.   4 10:17 /proc/sys/vm/max_map_count
#	root@ambre:/media/data/gobutler/assistant/elk# echo 262144 >/proc/sys/vm/max_map_count
#	root@ambre:/media/data/gobutler/assistant/elk# cat /proc/sys/vm/max_map_count
#	262144
#
# - sudo for docker commands ?
# - check for npm
# - check go version : 1.12 at least
# - for all go binaries, handle the cross compilation
###################################

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIR := $(dir $(MKFILE_PATH))

# Go parameters
GOPATH=${HOME}/go:$(shell pwd)
export GOPATH
GOCMD=go
GODEBUG=
GOINSTALL=$(GOCMD) install
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BIN_DIR=$(MKFILE_DIR)/dist

# Check some tools are present or not
DOCKER_BIN := $(shell command -v docker 2> /dev/null)
DOCKER_COMPOSE_BIN := $(shell command -v docker-compose 2> /dev/null)

# Docker labelling
DOCKER_LABEL="Assistant"

# Clean, get dependencies, test and build
all: clean deps test build

### Make entries

# Help
help:
	@awk '/^#/{c=substr($$0,3);next}c&&/^[[:alpha:]][[:alnum:]_-]+:/{print substr($$1,1,index($$1,":")),c}1{c=0}' $(MAKEFILE_LIST) | column -s: -t

# Build main components - server, client local, client websocket, vue.js webgui, webgui server
build: 
	@echo "############################################"
	@echo "# Build"
	@echo "############################################"

	# ~~~~ server ~~~~
	$(call build_server)

	# ~~~~ client local ~~~~
	$(call build_client_local)
	
	# ~~~~ client websocket ~~~~
	$(call build_client_websocket)

	# ~~~~ GUI webgui ~~~~
	$(call build_webgui)

	# ~~~~ client web gui ~~~~
	$(call build_client_webgui)

	@echo "End with success :)"

# Build the server
server: 
	@echo "############################################"
	@echo "# Build server"
	@echo "############################################"
	$(call build_server)

	@echo "End with success :)"

# Build the webgui server (but not the vue.js webgui part)
webgui: 
	@echo "############################################"
	@echo "# Build webgui (without building the GUI"
	@echo "############################################"
	$(call build_client_webgui)

	@echo "End with success :)"


# Execute the tests
test: 
	@echo "############################################"
	@echo "# Test"
	@echo "############################################"
	$(GOTEST) -v ./...


### Functions

# Configuration for components with Brain embedded in it
BRAIN_CONFIG_SAMPLE=./src/brain-config.yml.sample
BRAIN_CONFIG_TARGET=brain-config.yml
PACKAGES_SRC=./src/packages


# Client local (brain included)
CLIENT_LOCAL_BIN=client_local
CLIENT_LOCAL_DIR=$(BIN_DIR)/client_local
define build_client_local
	mkdir -p $(CLIENT_LOCAL_DIR)

	mkdir -p $(CLIENT_LOCAL_DIR)/packages
	cp -Rp $(PACKAGES_SRC)/* $(CLIENT_LOCAL_DIR)/packages/

	cp -Rp $(BRAIN_CONFIG_SAMPLE) $(CLIENT_LOCAL_DIR)/
	[ ! -f $(CLIENT_LOCAL_DIR)/$(BRAIN_CONFIG_TARGET) ] && cp -Rp $(BRAIN_CONFIG_SAMPLE) $(CLIENT_LOCAL_DIR)/$(BRAIN_CONFIG_TARGET) || true

	GOBIN=$(CLIENT_LOCAL_DIR)/ $(GOINSTALL) $(GODEBUG) assistant/clients/client_local

	ls -ltrh $(CLIENT_LOCAL_DIR)/$(CLIENT_LOCAL_BIN)
endef

# Client : websocket sample
CLIENT_WEBSOCKET_BIN=client_websocket
CLIENT_WEBSOCKET_DIR=$(BIN_DIR)/client_websocket
define build_client_websocket
	mkdir -p $(CLIENT_WEBSOCKET_DIR)
	GOBIN=$(CLIENT_WEBSOCKET_DIR)/ $(GOINSTALL) $(GODEBUG) assistant/clients/client_websocket
	ls -ltrh $(CLIENT_WEBSOCKET_DIR)/$(CLIENT_WEBSOCKET_BIN)
endef

# GUI : webgui
define build_webgui
	#./src/webgui/build.sh
	#ls -ltrh ./src/webgui/dist/index.html
endef

# Client : webgui
CLIENT_WEBGUI_TEMPLATES=./src/assistant/clients/webguiserver/templates/* 
CLIENT_WEBGUI_VUE_FILES=./src/webgui/dist/* 
CLIENT_WEBGUI_BIN=webguiserver
CLIENT_WEBGUI_DIR=$(BIN_DIR)/webgui
CLIENT_WEBGUI_DOCKER_IMAGE=assistant-webgui
define build_client_webgui
	mkdir -p $(CLIENT_WEBGUI_DIR)

	mkdir -p $(CLIENT_WEBGUI_DIR)/templates
	cp -Rp $(CLIENT_WEBGUI_TEMPLATES) $(CLIENT_WEBGUI_DIR)/templates/

	# Grab files from webgui project
	mkdir -p $(CLIENT_WEBGUI_DIR)/web
	cp -Rp $(CLIENT_WEBGUI_VUE_FILES) $(CLIENT_WEBGUI_DIR)/web/

	GOBIN=$(CLIENT_WEBGUI_DIR)/ $(GOINSTALL) $(GODEBUG) assistant/clients/webguiserver
	ls -ltrh $(CLIENT_WEBGUI_DIR)/$(CLIENT_WEBGUI_BIN)
endef

# Assistant Server (brain included)
SERVER_TEMPLATES=./src/assistant/server/templates/* 
SERVER_BIN=server
SERVER_DIR=$(BIN_DIR)/server
SERVER_DOCKER_IMAGE=assistant-server
define build_server
	mkdir -p $(SERVER_DIR)

	mkdir -p $(SERVER_DIR)/templates
	cp -Rp $(SERVER_TEMPLATES) $(SERVER_DIR)/templates/

	mkdir -p $(SERVER_DIR)/packages
	cp -Rp $(PACKAGES_SRC)/* $(SERVER_DIR)/packages/

	cp -Rp $(BRAIN_CONFIG_SAMPLE) $(SERVER_DIR)/
	[ ! -f $(SERVER_DIR)/$(BRAIN_CONFIG_TARGET) ] && cp -Rp $(BRAIN_CONFIG_SAMPLE) $(SERVER_DIR)/$(BRAIN_CONFIG_TARGET) || true

	GOBIN=$(SERVER_DIR)/ $(GOINSTALL) $(GODEBUG) assistant/server
	ls -ltrh $(SERVER_DIR)/$(SERVER_BIN)
endef



# Clean all builded files
clean: 
	@echo "############################################"
	@echo "# Clean"
	@echo "############################################"
	$(GOCLEAN)

	# Clean client : local
	rm -f $(CLIENT_LOCAL_DIR)/*.log
	rm -f $(CLIENT_LOCAL_DIR)/*sample
	rm -f $(CLIENT_LOCAL_DIR)/$(CLIENT_LOCAL_BIN)*
	rm -Rf $(CLIENT_LOCAL_DIR)/packages

	# Clean client : websocket sample
	rm -f $(CLIENT_WEBSOCKET_DIR)/*.log
	rm -f $(CLIENT_WEBSOCKET_DIR)/$(CLIENT_WEBSOCKET_BIN)*

	# Clean GUI : web gui
	# TODO
	
	# Clean client : web gui
	rm -f $(CLIENT_LOCAL_DIR)/*.log
	rm -f $(CLIENT_LOCAL_DIR)/$(CLIENT_WEBGUI_BIN)*
	rm -f $(CLIENT_WEBGUI_DIR)/ssl_webgui*
	rm -Rf $(CLIENT_WEBGUI_DIR)/templates
	rm -Rf $(CLIENT_WEBGUI_DIR)/static
	rm -Rf $(CLIENT_WEBGUI_DIR)/web

	# Clean server 
	rm -f $(SERVER_DIR)/*.log
	rm -f $(SERVER_DIR)/*sample
	rm -f $(SERVER_DIR)/$(SERVER_BIN)*
	rm -Rf $(SERVER_DIR)/packages
	rm -f $(SERVER_DIR)/ssl_server*
	rm -Rf $(SERVER_DIR)/templates

	# TODO : clean xml files (which ones ??? don't remember)
	# TODO : don't clean the output files!
	
	# if docker binary exists
	# TODO : do as root
	# If Docker, clean docker images and containers
	# clean stopped containers
	#sudo docker container prune -f --filter "label=project=$(DOCKER_LABEL)"  # The Doockerfile sets the label
	# clean images
	#sudo docker image prune -f -a --filter "label=project=$(DOCKER_LABEL)"





# Download dependencies
deps:
	@echo "############################################"
	@echo "# Deps"
	@echo "############################################"
	# Logging
	${GOGET} github.com/sirupsen/logrus
	
	# Improved http router
	${GOGET} github.com/gorilla/mux

	# YAML parsing
	${GOGET} gopkg.in/yaml.v3

	# UUID
	${GOGET} github.com/lithammer/shortuuid

	# WebSocket
	${GOGET} github.com/gorilla/websocket

	# Service discovery
	${GOGET} github.com/schollz/peerdiscovery

	# JWT
	${GOGET} github.com/dgrijalva/jwt-go

	# Go-cache
	${GOGET} github.com/patrickmn/go-cache


# Build the docker images
docker_build:
ifndef DOCKER_BIN
	$(error "Docker is not installed. Please install it")
endif
	@echo "############################################"
	@echo "# Docker : build images"
	@echo "# - 2 servers"
	@echo "# - 1 webgui"
	@echo "############################################"

	### Prepare the Dockerfiles
	@# Note that for a better experience with volume sharing in docker, each application has its own folder with the binary name
	# webgui
	@sed -e 's|{DIR}|.|' \
		-e 's|{LABEL}|$(DOCKER_LABEL)|' \
		-e 's|{BINARY}|$(CLIENT_WEBGUI_BIN)|' \
		-e 's|{APPNAME}|$(CLIENT_WEBGUI_BIN)|' \
		-e 's|{EXPOSE_PORT}|EXPOSE 8443|' \
		Dockerfile.tpl > $(CLIENT_WEBGUI_DIR)/Dockerfile
	
	# server 'lambda' : the 2 servers will use the same docker image
	@sed -e 's|{DIR}|.|' \
		-e 's|{LABEL}|$(DOCKER_LABEL)|' \
		-e 's|{BINARY}|$(SERVER_BIN)|' \
		-e 's|{APPNAME}|$(SERVER_BIN)|' \
		-e 's|{EXPOSE_PORT}||' \
		Dockerfile.tpl > $(SERVER_DIR)/Dockerfile

	### Build the docker images
	# webgui
	(cd $(CLIENT_WEBGUI_DIR)/ ; docker build -t $(CLIENT_WEBGUI_DOCKER_IMAGE) .)
	
	# server
	(cd $(SERVER_DIR)/ ; docker build -t $(SERVER_DOCKER_IMAGE) .)

	# Run the docker images
	@echo "Sample commands to run the images : "
	#docker run -p 8443:8443 $(CLIENT_WEBGUI_DOCKER_IMAGE) 
	#docker run $(SERVER_DOCKER_IMAGE) 

	@echo "End with success :)"

# Run the docker images
docker_run:
ifndef DOCKER_COMPOSE_BIN
	$(error "Docker compose is not installed. Please install it")
endif
	@echo "############################################"
	@echo "# Docker : run images"
	@echo "############################################"

	docker-compose up
