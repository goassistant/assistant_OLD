// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import 'es6-promise/auto'; // For VueX on IE
import Vuex from 'vuex';
import VueNativeSock from 'vue-native-websocket';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'




// Display img with basic authentication (for example : mjpeg cameras)
import VueAuthImage from 'vue-auth-image';

// Font-awesome
//import { library } from '@fortawesome/fontawesome-svg-core';
//import { faComments } from '@fortawesome/free-solid-svg-icons';
//import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// Font-awesome for vuetify
//import '@fortawesome/fontawesome-free/css/all.css';

// Data formatters
import moment from 'moment';

// Global css
import '@/assets/css/global.css';

// Local
import App from './App';
import router from './router';

// Data formatters
Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm');
  } 
  return '';
});

Vue.use(Vuex);
Vue.use(Vuetify);


// Display img with basic authentication (for example : mjpeg cameras)
Vue.use(VueAuthImage);

// Font-awesome : import only needed icons
//library.add(faComments);
//Vue.component('font-awesome-icon', FontAwesomeIcon);


// Store //////////////////////////
const store = new Vuex.Store({
  state: {
    chatHistory: [], // the history of the chat with the assistant
    peers: [], // the list of the available peers
    servers: [], // the list of the available servers
    socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
    },
  },
  getters: {
    CHAT_HISTORY: state => {
      return state.chatHistory;
    },
    PEERS: state => {
      return state.peers;
    },
    SERVERS: state => {
      return state.servers;
    },
    MESSAGE: state => {
      return state.socket.message;
    },
    IS_CONNECTED: state => {
      return state.socket.isConnected;
    },
  },
  mutations: {
    SOCKET_ONOPEN(state, event) {
      Vue.prototype.$socket = event.currentTarget;
      state.socket.isConnected = true;
      console.log('WS : connected');
    },
    SOCKET_ONCLOSE(state, event) {
      state.socket.isConnected = false;
    },
    SOCKET_ONERROR(state, event) {
      console.error(state, event);
      // TODO : how to check if this is an authentication error ?
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE(state, message) {
      state.socket.message = message;
      console.log('WS : received message : ', message);
      if (message.type === 'dialog') {
        if (!message.ack) { // we should not see them, but just in case, skip the ack messages
          state.chatHistory.push(message);
        }
      } else if (message.type === 'servers') {
          state.servers = message.servers;
      } else if (message.type === 'peers') {
          state.peers = message.peers;
          console.log('PEERS!!!');
      }
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
      console.info(state, count);
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
    ADD_TO_CHAT_HISTORY(state, message) {
      state.chatHistory.push(message);
    },
  },
  actions: {
    sendMessage(context, message) {
      message.token = localStorage.getItem('token');
      console.log('WS : send message : ', message);
      Vue.prototype.$socket.sendObj(message);
      this.commit('ADD_TO_CHAT_HISTORY', message);
    },
  },
});


// Websockets //////////////////////
let wssUrl = '';
let authenticateUrl = '';
let proxyUrl = '';
if (process.env.ASSISTANT_ADDRESS !== undefined) {
  wssUrl = `wss://${process.env.ASSISTANT_ADDRESS}/ws/gateway`;
  authenticateUrl = `https://${process.env.ASSISTANT_ADDRESS}/authenticate`;
  proxyUrl = `https://${process.env.ASSISTANT_ADDRESS}/proxy`;
} else {
  wssUrl = `wss://${location.host}/ws/gateway`;
  authenticateUrl = `https://${location.host}/authenticate`;
  proxyUrl = `https://${location.host}/proxy`;
}
export default {
  wssUrl,
  authenticateUrl,
  proxyUrl,
};

// Even if not yet authenticated, init the websocket connextion for now...
// TODO : improve this later on ?
Vue.use(VueNativeSock,
  wssUrl, {
    store,
    format: 'json',
    reconnection: true, // (Boolean) whether to reconnect automatically (false)
    // reconnectionAttempts: 50, // (Number) number of connection attempts before giving up (Infinity)
    reconnectionDelay: 5000, // (Number) how long to initially wait before attempting a new (1000)
  });


// Init Vue App ////////////////////

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  vuetify: new Vuetify({
    theme: {
      dark: true,
    },
    icons: {
      iconfont: 'md',
    },
  }),
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
});
