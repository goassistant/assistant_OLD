import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login';
import Layout from '@/components/Layout';
import Home from '@/components/Home';
import NotFound from '@/components/NotFound';
import Server from '@/components/Server';
import ProcessorMotionDetection from '@/components/ProcessorMotionDetection';
import ProcessorMotionDetectionPreview from '@/components/ProcessorMotionDetectionPreview';
import ProcessorOcr from '@/components/ProcessorOcr';

Vue.use(Router);


// TODO : https://serversideup.net/building-page-layout-vue-router/

const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/',
      name: 'Layout',
      component: Vue.component('Layout', Layout),
            children: [
                {
                    path: '/',
                    name: 'Home',
                    component: Vue.component('Home', Home),
                },
                {
                    path: '/server/:address',
                    name: 'Server',
                    component: Vue.component('Server', Server),
                },
                {
                    path: '/motion.detection/:address',
                    name: 'MotionDetection',
                    component: Vue.component('MotionDetection', ProcessorMotionDetection),
                },
                {
                    path: '/motion.detection/:address/preview/:previewId',
                    name: 'MotionDetectionPreview',
                    component: Vue.component('MotionDetectionPreview', ProcessorMotionDetectionPreview),
                },
                {
                    path: '/ocr/:address',
                    name: 'Ocr',
                    component: Vue.component('Ocr', ProcessorOcr),
                },
            ],
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
    },
  ],
});
export default router;

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path); // auth required for all pages except public pages
  const loggedIn = localStorage.getItem('token');

  if (authRequired && !loggedIn) {
    return next({
      path: '/login',
      query: { returnUrl: to.path },
    });
  }

  return next();
});
