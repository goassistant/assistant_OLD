package security

import (
	"assistant/logging"
)

var Component string = "Security"
var Version string = "1"
var log logging.LoggingContext

var AssistantSecret = "secret"

func init() {
	// Set logger
	log = logging.NewLogger(Component, Version)
}
