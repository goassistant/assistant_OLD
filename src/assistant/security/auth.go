package security

import (
	//"encoding/base64"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	//"strings"
)

type handler func(w http.ResponseWriter, r *http.Request)

// AddCorsHeaders()
// Enable CORS on some urls
func AddCorsHeaders(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH")
	(*w).Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
}

/*
// CorsAndJwtAuth()
// Check if the handler can be accessed or not
// Also handle the CORS things : headers, preflight call as OPTIONS
// More informations about CORS on https://dev.to/effingkay/cors-preflighted-requests--options-method-3024
// More informations about JWT on https://www.thepolyglotdeveloper.com/2017/03/authenticate-a-golang-api-with-json-web-tokens/
func CorsAndJwtAuth(pass handler) handler {
	// TODO
	// TODO
	// TODO
	return nil
}

// CorsAndBasicAuth()
// Check if the handler can be accessed or not
// Also handle the CORS things : headers, preflight call as OPTIONS
// More informations about CORS on https://dev.to/effingkay/cors-preflighted-requests--options-method-3024
func CorsAndBasicAuth(pass handler) handler {
	return func(w http.ResponseWriter, r *http.Request) {
		// Enable CORS for all urls with Basic Authentication
		AddCorsHeaders(&w)
		if r.Method == "OPTIONS" {
			// Handle the preflight request
			// - we will always respond HTTP 200 with "Preflight allowed"
			// - no check on user credentials as this is only a preflight request
			log.Debug("Preflight request received. Responding HTTP 200")
			w.Write([]byte("allowed"))
			return
		}

		// TODO : debug, delete
		log.Debug("Request Headers :")
		for name, headers := range r.Header {
			name = strings.ToLower(name)
			for _, h := range headers {
				log.Debug(fmt.Sprintf("  %v: %v", name, h))
			}
		}

		auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)

		if len(auth) != 2 || auth[0] != "Basic" {
			http.Error(w, "Authorization failed (E1)", http.StatusUnauthorized)
			return
		}

		payload, _ := base64.StdEncoding.DecodeString(auth[1])
		pair := strings.SplitN(string(payload), ":", 2)

		if len(pair) != 2 || !ValidateUser(pair[0], pair[1]) {
			http.Error(w, "Authorization failed (E2)", http.StatusUnauthorized)
			return
		}

		pass(w, r)
	}
}
*/

// ValidateUser()
// Check if the user/password provided is appropriate or not
func ValidateUser(username, password string) bool {
	res := false
	if username == "test" && password == "Turtle9!" {
		res = true
	}
	log.Debug(fmt.Sprintf("HTTP authentication: username='%s', password='%s': status='%t'", username, password, res))
	return res
}

// ValidateToken()
// Check if the given token is validate
func ValidateToken(tokenString string) bool {
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(AssistantSecret), nil
	})
	if err != nil {
		log.Error(fmt.Sprintf("Error while decofing JWT Token '%s' : '%+v'", tokenString, err))
		return false
	}

	// Debug
	//for key, val := range claims {
	//	fmt.Printf("Key: %v, value: %v\n", key, val)
	//}

	// Validate the content of the decoded claims
	return ValidateUser(claims["username"].(string), claims["password"].(string))

}
