# Websocket client sample

This client is a sample to show how to connect to the servers over websockets. It will : 

- execute the discovery process in background.
- when a server is discovered, add it to the list of the discovered servers and allow to discuss with it.
