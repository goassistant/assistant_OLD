package main

/* Assistant web socket client

This program is used to quickly test the brain server over websockets
*/

import (
	"assistant/brain"
	"assistant/logging"
	"assistant/tools"
	"assistant/websocketclient"
	"bufio"
	"fmt"
	"os"
	"strings"
)

var Component string = "WebSocket client"
var Version string = "0.1"

var log logging.LoggingContext

func main() {
	// Set logger
	log = logging.NewLogger(Component, Version)
	log.Info(fmt.Sprintf("[ %s (%s) ]", Component, Version))

	// Start the chat
	readStdInForever()

}

func processResponse(data brain.BrainExchange) {
	// Process the response

	// Check if this is a dialog message and not a ACK message
	// This is one on client side because we may want to emit some signal when an ack is received
	if data.Type == "dialog" && data.Ack == false {
		fmt.Println("\n" + data.Name + " > " + data.Text + "\n")
	}
}

func readStdInForever() {
	log.Info("Wait for servers discovery...")
	// Wait for servers discovery
	websocketclient.WaitForOnePeerDiscovered()

	// Init the stdin
	reader := bufio.NewReader(os.Stdin)

	// Set the response handler
	websocketclient.SetResponseHandler(processResponse)

	fmt.Print("\n\nTo interact with the assistant, just type some text and press ENTER.\nTo quit, use CTRL-C.\n\n")
	// Eternal loop to read input and process it
	for {
		// read input
		text, _ := reader.ReadString('\n')
		fmt.Print("\n")
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)

		if text == "" {
			continue
		}

		data := brain.BrainExchange{
			Type: "dialog",
			Text: text,

			// TODO : do it more clean ?
			Name: websocketclient.ComputerUsername, // the username of the user on the machine

			RequestId: tools.GetShortUUID(),
			ClientId:  "wsLocal", // this one is hardcoded as we have only 1 local client at time
		}

		// For advanced debug only
		//log.Debug(fmt.Sprintf("Sending Json data to the server(s) :"))
		//log.PrettyPrint(data)

		// Write to the servers channels
		websocketclient.PublishRequest(data)

	}
}
