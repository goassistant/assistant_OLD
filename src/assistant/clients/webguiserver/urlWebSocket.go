package main

import (
	"assistant/brain"
	"assistant/security"
	"assistant/tools"
	"assistant/websocketclient"
	"crypto/tls"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"net/http/httputil"
	"net/url"
	"reflect"
	"strings"
	"time"
)

// Upgrader for the websocket to upgrade from HTTP to TCP
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// The list of the websocket objects to help the function processResponse to communicate the brain
// response on the appropriate websocket channel
var webSocketsChannels = make(map[string]*(websocket.Conn))

func wsGatewayEndpoint(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	// Get an unique id for the client
	wsClientId := tools.GetShortUUID()

	// upgrade this connection to a WebSocket
	// connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error(fmt.Sprintf("[ user %s ] Websocket : %v", wsClientId, err))
	}

	log.Info(fmt.Sprintf("[ user %s ] Websocket : New client connected", wsClientId))

	// listen indefinitely for new messages coming
	// through on our WebSocket connection
	wsReader(ws, wsClientId)
}

// wsReader()
// Listen eternally on websocket to read data on it
func wsReader(conn *websocket.Conn, wsClientId string) {
	////// First, set up the websocket client (websocket client to all assistant servers)

	// Wait for servers discovery
	// websocketclient.WaitForOnePeerDiscovered()
	// TODO : really needed here ? Maybe... not ?

	// Set the assistant response handler
	websocketclient.SetResponseHandler(processResponse)

	// Store the websocket channel for the client id
	webSocketsChannels[wsClientId] = conn

	// Send the viewed servers list to the client when there are some updates
	// And also send the peers list
	stopchan := make(chan bool)
	go func() {
		versionOldServers := -1
		versionOldPeers := -1
		for {
			select {
			default:
				// servers
				vServers, versionServers := websocketclient.GetViewedServers()
				if versionServers != versionOldServers {
					// Send only the information when there is some update
					log.Debug(fmt.Sprintf("[ user %s ] Writing JSON : %+v", wsClientId, vServers))
					//log.PrettyPrint(vServers)
					err := conn.WriteJSON(vServers)
					if err != nil {
						log.Error(fmt.Sprintf("[ user %s ] ] writeJson: %v", wsClientId, err))
					}
				}
				versionOldServers = versionServers

				// peers
				peers, versionPeers := websocketclient.GetPeers()
				if versionPeers != versionOldPeers {
					// Rebuild proxypass routing for peers only when there is some update
					// TODO - find a way to remove useless routes...
					for _, peer := range peers.Peers {
						newRoute := fmt.Sprintf("/proxy/%s", peer.Address)
						exists := false
						// log.Debug(fmt.Sprintf("%+v", http.DefaultServeMux))
						// => &{mu:{w:{state:0 sema:0} writerSem:0 readerSem:0 readerCount:0 readerWait:0} m:map[/:{h:0x693270 pattern:/} /authenticate:{h:0x6f1f30 pattern:/authenticate} /proxy/192.168.1.50:44957:{h:0x6f1f30 pattern:/proxy/192.168.1.50:44957} /ws/gateway:{h:0x6f28d0 pattern:/ws/gateway}] es:[{h:0x693270 pattern:/}] hosts:false}
						t1 := reflect.ValueOf(*http.DefaultServeMux)
						t2 := t1.FieldByName("m")
						for _, v := range t2.MapKeys() {
							existingRoute := fmt.Sprintf("%v", v)
							if existingRoute == newRoute {
								exists = true
							}
						}
						if !exists {
							remote, err := url.Parse(fmt.Sprintf("https://%s", peer.Address))
							if err != nil {
								log.Error(fmt.Sprintf("%v", err))
								continue
							}

							// Inspired from : https://www.integralist.co.uk/posts/golang-reverse-proxy/

							proxy := httputil.NewSingleHostReverseProxy(remote)
							// Allow not checked ssl certificates
							proxy.Transport = &http.Transport{
								TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
							}
							// Configure proxy
							addr := peer.Address // to avoid a bug like this :
							//  DEBUG[....] Url call : 192.168.1.50:50616 GET /proxy/192.168.1.50:41389/info
							//  req.URL.Host = 192.168.1.50:41389   // good
							//  DEBUG[....] Url call : 192.168.1.50:50616 GET /proxy/192.168.1.50:41375/info
							//  req.URL.Host = 192.168.1.50:41389   // not good !!! should be 41375

							proxy.Director = func(req *http.Request) {
								// ??? originHost := conf.Host
								// ??? req.Header.Add("X-Forwarded-Host", req.Host)
								// ??? req.Header.Add("X-Origin-Host", originHost)
								// ??? req.Host = originHost

								// TODO : remove debug line
								fmt.Printf("req.URL.Host = %v\n", addr)

								// TODO : if it does not work, get the host from the req.URL.Path content...
								req.URL.Host = addr // host:port in fact :)

								req.URL.Scheme = "https"

								// Remove /proxy/xxxx from url
								req.URL.Path = strings.Replace(req.URL.Path, newRoute, "", -1)
							}
							log.Debug(fmt.Sprintf("http : add route %s => %s", newRoute, peer.Address))
							http.HandleFunc(newRoute, proxyHandler(proxy))
							s := router.PathPrefix(newRoute).Subrouter()
							s.HandleFunc("/{path:.*}", proxyHandler(proxy))
						}
					}

					// Send only the information when there is some update
					log.Debug(fmt.Sprintf("[ user %s ] Writing JSON : %+v", wsClientId, peers))
					//log.PrettyPrint(vServers)
					err := conn.WriteJSON(peers)
					if err != nil {
						log.Error(fmt.Sprintf("[ user %s ] ] writeJson: %v", wsClientId, err))
					}
				}
				versionOldPeers = versionPeers

				// wait a little
				time.Sleep(1000 * time.Millisecond)

			case <-stopchan:
				// stop
				log.Debug(fmt.Sprintf("[ user %s ] Stop sending servers informations as connection closed", wsClientId))
				return

			}
		}
	}()

	////// Then, we can start listening websocket
	for {
		// read in a json message
		var request brain.BrainExchange
		err := conn.ReadJSON(&request)
		if err != nil {
			// if error, it could be because of :
			// - a closed connection
			// - some non json data transmitted : it seems to finish in a closed connection
			log.Error(fmt.Sprintf("[ user %s ] Websocket > received : %v", wsClientId, err))
			stopchan <- true
			return
		}

		// Check the token given
		tokenValid := security.ValidateToken(request.Token)
		if !tokenValid {
			// Invalid token.
			// Just ignore the request
			log.Warn(fmt.Sprintf("Invalid token '%s'", request.Token))
			return
		}

		// complete request
		request.RequestId = tools.GetShortUUID()
		request.ClientId = wsClientId

		// handle messages
		log.Debug(fmt.Sprintf("[ user %s ] Websocket > received from user : %+v", wsClientId, request))
		//log.PrettyPrint(request)

		// Use the websocket client
		websocketclient.PublishRequest(request)

	}
}

// processResponse()
// Process the response received by one of the assistant servers (the first to respond)
func processResponse(data brain.BrainExchange) {
	// Process the response

	// Check if this is a dialog message and not a ACK message
	// This is one on client side because we may want to emit some signal when an ack is received
	if data.Type == "dialog" && data.Ack == false {
		log.Debug(fmt.Sprintf("[ user %s ] Writing JSON : %+v", data.ClientId, data))

		conn := webSocketsChannels[data.ClientId]

		err := conn.WriteJSON(data)
		if err != nil {
			log.Error(fmt.Sprintf("[ user %s ] ] writeJson: %v", data.ClientId, err))
		}
	}
}

// proxyHandler()
//
func proxyHandler(p *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// To avoid duplicate values when returning the response, we clean up the headers.
		// Because, they are set twice in normal usage :
		// - by the remote peer
		// - by the log.HttpRequest function
		// And stangely we can't read these headers in log.HttpRequest and so handle duplicates...
		// So we do it here!
		w.Header().Del("access-control-allow-origin")
		w.Header().Del("access-control-allow-methods")
		w.Header().Del("access-control-allow-headers")
		// Serve
		p.ServeHTTP(w, r)
	}
}
