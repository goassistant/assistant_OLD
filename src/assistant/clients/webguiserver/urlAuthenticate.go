package main

import (
	"assistant/security"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type JwtToken struct {
	Token string `json:"token"`
}

// httpAuthentication()
// A simple call to authenticate and get the JWT Token
// Example of input :
// {"username" : "mylogin", "password" : "mypassword"}
// Example of response :
// {"token":"eyJhbxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx..."}
func httpAuthentication(w http.ResponseWriter, r *http.Request) {
	// Enable CORS for all urls with Basic Authentication
	security.AddCorsHeaders(&w)
	if r.Method == "OPTIONS" {
		// Handle the preflight request
		// - we will always respond HTTP 200 with "Preflight allowed"
		// - no check on user credentials as this is only a preflight request
		log.Debug("Preflight request received. Responding HTTP 200")
		w.Write([]byte("allowed"))
		return

	} else if r.Method == "POST" {
		// Handle the login action
		var user User
		err := json.NewDecoder(r.Body).Decode(&user)
		if err != nil {
			log.Warn(fmt.Sprintf("Authentication: invalid no json data received: '%s'", r.Body))
		}

		if security.ValidateUser(user.Username, user.Password) {
			log.Info(fmt.Sprintf("Authentication: success for: '%s'", user.Username))
			// Credentials ok, give the token !
			// TODO : move in security/auth.go
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"username": user.Username,
				"password": user.Password,
			})
			tokenString, error := token.SignedString([]byte(security.AssistantSecret))
			if error != nil {
				fmt.Println(error)
			}
			json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
			// something like this : {"token":"eyJhbGcxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.eyJwxxxxxxxxxxxxxxxxxxxxxxxxxxxxxXNlcm5hbWUiOiJ0ZXN0In0.T6bxxxxxxxxxxxxxxxxxxxxxxxxxxxxx4bxQ94GpNnU"}
			return
		} else {
			log.Warn(fmt.Sprintf("Authentication : invalid credentials"))
			http.Error(w, "Authorization failed (E1)", http.StatusUnauthorized)
			return
		}
	} else {
		http.Error(w, "Authorization failed (E0)", http.StatusUnauthorized)
		return

	}
}
