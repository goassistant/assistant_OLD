package main

import (
	"assistant/logging"
	"assistant/security"
	"assistant/tools"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

var Component string = "Assistant Web Gui Server"
var Version string = "0.1"
var Role string = "gateway"
var Name string = ""
var Address string = ""
var Hostname string = ""
var defaultHttpsPort int = 8443
var sslCrtFile string = "ssl_webgui.crt"
var sslKeyFile string = "ssl_webgui.key"

var log logging.LoggingContext

// Init mux router
// It is global because we need to dynamically add routes to proxy to peers
var router = mux.NewRouter()

func main() {
	// Parameters
	var portPtr = flag.Int("port", defaultHttpsPort, "Listening port")
	flag.Parse()

	// Set logger
	log = logging.NewLogger(Component, Version)

	// Static files
	// In static files, there is the website. We publish it as /
	// It is easy to do with net/http...
	// But with mux (gorilla), if you serve as /, it will overwrite all the other urls...
	// So we will serve part by part!
	// This avoid to use the very long function in Gorilla doc!
	// 1. the index.html file
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./web/index.html")
	})
	// 2. the static files
	fs := http.Dir("web/static")
	router.PathPrefix("/static").Handler(http.StripPrefix("/static", http.FileServer(fs)))

	// Routing
	router.HandleFunc("/info", httpInfo)
	// Simple Auth only to get the JWT Token
	router.HandleFunc("/authenticate", httpAuthentication)
	// JWT Token for all remaining urls
	// The token will be included as key 'token' in all sent websocket messages
	router.HandleFunc("/ws/gateway", wsGatewayEndpoint) // webSocket

	// Proxy
	// Some proxy routes are dynamically added in urlWebSocket.go when new peers are detected

	// Apply mux router
	http.Handle("/", router)

	// Check if the ssl required files exist.
	weNeedToGenerateCertificateAndKeyForHttps := false
	if _, err := os.Stat(sslCrtFile); os.IsNotExist(err) {
		weNeedToGenerateCertificateAndKeyForHttps = true
	}
	if _, err := os.Stat(sslKeyFile); os.IsNotExist(err) {
		weNeedToGenerateCertificateAndKeyForHttps = true
	}

	// If not, create them
	if weNeedToGenerateCertificateAndKeyForHttps {
		security.GenerateCertificateAndKey(sslCrtFile, sslKeyFile)
	}

	// TODO : later on, handle the possibility to call other external servers
	//        (public servers for weather, jokes, ... for example)

	// Init the webSocketsChannels
	//initWebSocketsChannels()

	// Get server hostname and name (role@hostname)
	Hostname, Name = tools.GetHostnameAndName(Role)

	// Get server ip adressess
	ip := tools.GetMainIP()

	// Execute the ssl_server
	port := *portPtr
	Address = fmt.Sprintf("%s:%d", ip, port) // also used by urlInfo.go
	log.Info(fmt.Sprintf("Starting HTTPS server on port %d", port))
	err := http.ListenAndServeTLS(fmt.Sprintf(":%d", port), sslCrtFile, sslKeyFile, log.HttpRequest(http.DefaultServeMux))
	if err != nil {
		log.Fatal(fmt.Sprintf("ListenAndServe: %v", err))
	}

	// Nothing will be executed here...
}
