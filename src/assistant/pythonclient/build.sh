#!/bin/bash
full_path=$(realpath $0)
dir_path=$(dirname $full_path)

echo "Upgrade python tools..."
python3 -m pip install --user --upgrade setuptools wheel

echo "Build package..."
cd $dir_path
python3 setup.py sdist bdist_wheel
cd -

echo "End"
