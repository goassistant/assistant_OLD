#!/bin/bash
full_path=$(realpath $0)
dir_path=$(dirname $full_path)

echo "Cleaning..."
rm -Rf $dir_path/dist
rm -Rf $dir_path/build
rm -Rf $dir_path/assistant_lib.egg-info
echo "... finished"
