import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="assistant_lib", 
    version="0.1",
    author="Fritz SMH",
    author_email="fritz.smh@gmail.com",
    description="Python library for the assistant",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/TODO/TODO",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
