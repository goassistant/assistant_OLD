#!/usr/bin/python3
#
# This script is used to deliver the assisant library package on a gitlab repository.

import requests
import json

TOKEN = 'j21se-ypD8SgRo-TsMfK'
PROJECT_ID = '15406167'
FILE = './dist/assistant_lib-0.1.tar.gz'
TAG = 'python-assistant-lib'

token_header = {'PRIVATE-TOKEN': TOKEN}

# 1. upload the file
print("Upload the file '{0}'".format(FILE))
url_upload = "https://gitlab.com/api/v4/projects/{0}/uploads".format(PROJECT_ID)
file = {"file": (FILE, open(FILE,'rb'), "Content-Type: multipart/form-data")}
a = requests.post(url_upload, files=file, headers=token_header)
print(a.text)
# Sample successfull response :
# {"alt":"assistant_lib-0.1.tar.gz","url":"/uploads/0a9cbf648e0522b8bbeca70bcbb21831/assistant_lib-0.1.tar.gz","markdown":"[assistant_lib-0.1.tar.gz](/uploads/0a9cbf648e0522b8bbeca70bcbb21831/assistant_lib-0.1.tar.gz)"}
response = json.loads(a.text)
print(response['url'])
print(response['markdown'])

# 2. create a release
print("Create the tag '{0}'".format(TAG))
url_release = "https://gitlab.com/api/v4/projects/{0}/repository/tags?tag_name={1}&ref=master".format(PROJECT_ID, TAG)
a = requests.post(url_release, headers=token_header)
print(a.text)
# Sample successfull response :
# {"name":"python-assistant-lib","message":"","target":"552d3dba2c0ea213ceb142cbb604af011b5758a9","commit":{"id":"552d3dba2c0ea213ceb142cbb604af011b5758a9","short_id":"552d3dba","created_at":"2019-11-18T20:50:19.000Z","parent_ids":["b74f6a0328f881a09413a5c0d234c351ab9c9377"],"title":"Update public/index.html","message":"Update public/index.html","author_name":"Fritz SMH","author_email":"fritz.smh@gmail.com","authored_date":"2019-11-18T20:50:19.000Z","committer_name":"Fritz SMH","committer_email":"fritz.smh@gmail.com","committed_date":"2019-11-18T20:50:19.000Z"},"release":null,"protected":false}
# {"message":"Tag python-assistant-lib already exists"}

print("Delete the release associated to the tag")
url_release = "https://gitlab.com/api/v4/projects/{0}/releases/{1}".format(PROJECT_ID, TAG)
a = requests.delete(url_release, headers=token_header)
print(a.text)

print("Create the release associated to the tag")
url_release = "https://gitlab.com/api/v4/projects/{0}/repository/tags/{1}/release".format(PROJECT_ID, TAG)
desc = {"description": "{0}".format(response['markdown'])}
a = requests.post(url_release, json=desc, headers=token_header)
print(a.text)
# Sample successfull response :
# {"tag_name":"python-assistant-lib","description":"Release"}
# {"message":"Release already exists"}
