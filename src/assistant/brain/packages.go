package brain

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

// Description of a package YML file
type packageFile struct {
	File     string
	Package  string
	Language string
}

// packagesYamlFiles()
// Returns the list of the packages yaml files
func packagesYamlFiles() []packageFile {
	pkgDir := PackagesFolder()
	log.Info(fmt.Sprintf("Listing packages files in '%s'...", pkgDir))
	var list []packageFile

	// Detect languages
	files, err := ioutil.ReadDir(pkgDir)
	if err != nil {
		log.Fatal(fmt.Sprintf("%v", err))
	}
	for _, f := range files {
		currentLanguage := f.Name()
		log.Info(fmt.Sprintf("- Language detected : %s", currentLanguage))

		// Detect packages
		files2, err := ioutil.ReadDir(pkgDir + "/" + currentLanguage)
		if err != nil {
			log.Fatal(fmt.Sprintf("%v", err))
		}
		for _, f2 := range files2 {
			currentPackage := f2.Name()
			log.Info(fmt.Sprintf("  - Package detected : %s", currentPackage))

			// Detect packages files
			files3, err := ioutil.ReadDir(pkgDir + "/" + currentLanguage + "/" + currentPackage)
			if err != nil {
				log.Fatal(fmt.Sprintf("%v", err))
			}
			for _, f3 := range files3 {
				// TODO : filter on '.yml' extension
				filename := f3.Name()
				log.Info(fmt.Sprintf("    - File : %s", filename))
				list = append(list, packageFile{filename, currentPackage, currentLanguage})
			}
		}
	}

	log.Info("Listing complete!")
	return list
}

// loadPackageInteractionsFromFile()
// Read and load interactions defined in a package yaml file.
func loadPackageInteractionsFromFile(language string, pkg string, filename string) {
	log.Info(fmt.Sprintf("Read interactions from the package file : %s > %s > %s", language, pkg, filename))

	yamlFile, err := ioutil.ReadFile(PackagesFolder() + "/" + language + "/" + pkg + "/" + filename)
	if err != nil {
		log.Error(fmt.Sprintf("Error while opening the package Yaml file : %v. Skipping the file", err))
		return
	}

	m := make(map[interface{}]interface{})
	err = yaml.Unmarshal(yamlFile, &m)
	if err != nil {
		log.Error(fmt.Sprintf("Error while parsing the package Yaml file : %v. Skipping the file", err))
		return
	}

	// Examples :
	// Yaml content :
	//  interactions:
	//    - when_text:
	//        - test sleep
	//        - do sleep test
	//      do:
	//        - text:
	//          - start sleeping for 10 seconds
	//          - I will go sleeping for 10 seconds
	//        - wait:
	//            time_s: 10
	//        - text:
	//          - stop sleeping
	//          - I just stopped sleeping
	// => interactions => [map[when_text:[test sleep do sleep test] do:[map[text:[start sleeping for 10 seconds I will go sleeping for 10 seconds]] map[wait:map[time_s:10]] map[text:[stop sleeping I just stopped sleeping]]]]]

	// fmt.Println(m["interactions"])
	// => [map[when_text:[test sleep do sleep test] do:[map[text:[start sleeping for 10 seconds I will go sleeping for 10 seconds]] map[wait:map[time_s:10]] map[text:[stop sleeping I just stopped sleeping]]]]]

	// Check if the file contains some interactions
	if m["interactions"] == nil {
		log.Info("No interaction in this file. Skipping it.")
		return
	}

	// Loop on all interactions
	interactionItems := m["interactions"].([]interface{})
	for i := 0; i < len(interactionItems); i++ {
		interactionId := fmt.Sprintf("%s-%s-%s-%d", language, pkg, filename, i)
		log.Debug(fmt.Sprintf("Interaction #%s", interactionId))

		// Please notice that when the file is read, the blocks can be processed in the wrong logical way :
		// priority => when_text => do
		// do => priority => when_text
		// ...
		// This is related to the fact that we are reading a dictionnary. And a dictionnary is not ordered ;)
		// This is not an issue, but this could be troublesome when reading the debug logs.

		// For an interaction, do a first loop to read some fields that must be processed before the remaining
		var priority uint16
		priority = 100 // default priority value
		for key, value := range interactionItems[i].(map[string]interface{}) {
			strKey := fmt.Sprintf("%v", key)
			if strKey == "priority" {
				priorityTmp, err := strconv.Atoi(fmt.Sprintf("%v", value))
				if err != nil {
					log.Error(fmt.Sprintf("Invalid priority value : %v. Should be [0..255]", priorityTmp))
				} else {
					priority = uint16(priorityTmp)
				}
			}
		}
		log.Debug(fmt.Sprintf("  - Priority = %d", priority))

		// For an interaction, process each part : 'when_text', 'do', ...
		for key, value := range interactionItems[i].(map[string]interface{}) {
			strKey := fmt.Sprintf("%v", key)
			//strValue := fmt.Sprintf("%v", value)

			// fmt.Println(strKey + " => " + strValue)

			if strKey == "priority" {
				// Already processed, do nothing

			} else if strKey == "when_text" {
				// BLOCK when_text
				log.Debug("  - Processing block WHEN_TEXT")
				// fmt.Printf("%T", value)
				// => []interface {}

				// The when_text part contains a list of strings
				for _, v := range value.([]interface{}) {
					text := fmt.Sprintf("%v", v)
					log.Debug(fmt.Sprintf("    - %s", text))

					// Process the when_text part
					addWhenTextEntry(text, priority, defaultSysPriority, language, pkg, interactionId)
				}

			} else if strKey == "do" {
				// BLOCK do
				log.Debug("  - Processing block DO")

				doActions := castInterfaceToSliceOfResponseAction(value)
				responses = append(responses, RequestResponse{
					interactionId,
					language,
					pkg,
					doActions,
				})

			} else {
				// BLOCK not handled...
				log.Error(fmt.Sprintf("Unknown block : %s", strKey))
			}

		}
	}

}

// castInterfaceToSliceOfResponseAction()
// This function can also be called from processor.go for some actions like 'execute_command'
func castInterfaceToSliceOfResponseAction(value interface{}) []ResponseAction {
	var doActions []ResponseAction
	// For a DO block, iterate over the list of do items
	for _, doItem := range value.([]interface{}) {

		// Convert map[interface]interface{} to map[string{}]interface{}
		// Depending on the structure of the DO block we need to process a type or another...
		// In the do block we have complex structure.
		// For example :
		//    do:
		//      - text:
		//        - "Hi"
		//        - "Hello"
		doItem2 := make(map[string]interface{})
		switch doItem.(type) {
		case map[interface{}]interface{}:
			for keyDoItem, valueDoItem := range doItem.(map[interface{}]interface{}) {
				doItem2[fmt.Sprintf("%v", keyDoItem)] = valueDoItem
			}
		case map[string]interface{}:
			for keyDoItem, valueDoItem := range doItem.(map[string]interface{}) {
				doItem2[fmt.Sprintf("%v", keyDoItem)] = valueDoItem
			}
		}

		doItem = doItem2
		//fmt.Printf("%v\n", doItem)
		for keyDoItem, valueDoItem := range doItem.(map[string]interface{}) {
			// keyDoItem example: 'text'
			// valueDoItem example: [text response 1, text response 2]
			log.Debug(fmt.Sprintf("    - do item type: %v", keyDoItem))
			log.Debug(fmt.Sprintf("    - do item parameters: %v", valueDoItem))
			log.Debug(fmt.Sprintf("    - do item parameters type: %T", valueDoItem))

			// Now, depending on the type of action to do, apply a different process
			switch keyDoItem {
			case
				"text",
				"wait",
				"redirect",
				"system",
				"execute_command",
				"video",
				"html":
				doActions = append(doActions, ResponseAction{
					keyDoItem,
					valueDoItem,
				})
			default:
				// Unknown or not handled type
				log.Error(fmt.Sprintf("Unknown action in the 'do' block : '%v' => '%v'", keyDoItem, valueDoItem))

			}

		}
	}
	return doActions

}

// loadPackageAliasesFromFile()
// Read and load aliases defined in a package yaml file.
func loadPackageAliasesFromFile(language string, pkg string, filename string) {
	log.Info(fmt.Sprintf("Read aliases from the package file : %s > %s > %s", language, pkg, filename))

	yamlFile, err := ioutil.ReadFile(PackagesFolder() + "/" + language + "/" + pkg + "/" + filename)
	if err != nil {
		log.Error(fmt.Sprintf("Error while opening the package Yaml file : %v. Skipping the file", err))
		return
	}

	m := make(map[interface{}]interface{})
	err = yaml.Unmarshal(yamlFile, &m)
	if err != nil {
		log.Error(fmt.Sprintf("Error while parsing the package Yaml file : %v. Skipping the file", err))
		return
	}

	// Check if the file contains some aliases
	if m["aliases"] == nil {
		log.Info("No alias in this file. Skipping it.")
		return
	}

	// Loop on all aliases
	alias := m["aliases"].([]interface{})
	var aliasName string
	for i := 0; i < len(alias); i++ {
		//fmt.Printf("%v", alias[i])
		//fmt.Printf("%T", alias[i])
		var tmpReplaceList []string
		for key, value := range alias[i].(map[string]interface{}) {
			//key := key.(string)
			if key == "name" {
				aliasName = value.(string)
				log.Debug(fmt.Sprintf("- alias name : %s", aliasName))
			} else if key == "replace" {
				//aliasReplace := value.([]interface{})
				tmpReplace := value.([]interface{})
				log.Debug(fmt.Sprintf("- alias replace items : %v", tmpReplace))
				for j := 0; j < len(tmpReplace); j++ {
					tmpReplaceList = append(tmpReplaceList, tmpReplace[j].(string))
				}
			}
		}
		aliases = append(aliases, Alias{language, aliasName, tmpReplaceList})
	}
	log.Debug(fmt.Sprintf("Loaded aliases (from all files) : %v", aliases))

}

// addWhenTextEntry()
// This function will process the "when_text" part of the package file.
func addWhenTextEntry(text string, userPriority uint16, sysPriority uint16, language string, pkg string, interactionId string) {
	// TODO : remove final . or !

	// Init the list of the text to add :
	var textList []string
	var newTextList []string
	// init the variable names list
	var vars []string
	// init some temp stuff
	var newText string = text // to keep the original text in case we need it

	// Set some system properties
	if newText == "*" {
		// All '*' entry should be processes at the very end :
		// If the favorita language if french, but we speak english, we need to parse all french and english input text
		// before switching to the '*' choice to say we didn't understand...
		sysPriority = 0
	}

	//--- all steps that applys on only the text input ---

	// Search for * and replace them by the appropriate regex part
	newText = replaceStarsForRegexp(newText)

	// Apply lower case
	// The lower case must be done before the processing of variables !
	// Else the regexp in the returned text could be invalide : (p<...>..) instead of (P<...>..)
	lowerText := strings.ToLower(newText)

	// Search for variables and process them.
	newText, vars = processVariables(lowerText, newText)

	// Add ^ and $ around the text
	newText = fmt.Sprintf("^%s$", newText)

	// Prepare the list of 1 input :)
	newTextList = nil
	textList = append(textList, newText)

	//--- all steps that can create several inputs from each input
	// Search for aliases and loop on each alias
	// 1. check if there is some "@" in the string.
	// 2. if so, loop over known aliases
	for t := 0; t < len(textList); t++ {
		newTextList = getAllSentencesFromAliases(textList[t], language)
	}
	textList = newTextList

	// TODO : process mandatory (), optionnal []

	// Add each string in the textInputs array
	for i := 0; i < len(textList); i++ {

		// First, get the related compiled regular expression
		r := getCompiledRegexp(textList[i])
		textInputs = append(textInputs, TextRequestInput{
			text,
			textList[i],
			r,
			userPriority,
			sysPriority,
			language,
			pkg,
			interactionId,
			vars,
		})
	}
}

// getAllSentencesFromAliases()
// Return a list of all the possibilities for the text related to the aliases
func getAllSentencesFromAliases(text string, language string) []string {
	var result []string
	var tmp []string

	if strings.Contains(text, "@@") {
		//fmt.Println("************")
		log.Debug("      @@ detected : there may be some aliases usage.")

		// init the work list with the initial text
		tmp = append(tmp, text)

		finished := false
		for finished != true {
			//fmt.Println("*** new loop")
			var tmpDest []string
			var foundAlias = false
			for i := 0; i < len(tmp); i++ {
				//fmt.Printf("=> %s\n", tmp[i])
				currentText := tmp[i]
				if strings.Contains(currentText, "@@") {
					foundAlias = true

					// search alias start
					openAliasIdx := strings.Index(currentText, "@@")
					textBeforeAlias := currentText[:openAliasIdx]
					//fmt.Println(textBeforeAlias)

					// search alias end
					tmpText := currentText[openAliasIdx+2:]
					closeAliasIdx := strings.Index(tmpText, "@@")
					textAfterAlias := tmpText[closeAliasIdx+2:]
					aliasName := tmpText[:closeAliasIdx]
					//fmt.Println(aliasName)
					//fmt.Println(textAfterAlias)

					// create 1 sentence for each alias replace
					for _, v := range aliases {
						if v.name == aliasName && language == v.language {
							//fmt.Println("Existing alias :)")
							for r := 0; r < len(v.replaceList); r++ {
								tmpDest = append(tmpDest, fmt.Sprintf("%s%s%s", textBeforeAlias, v.replaceList[r], textAfterAlias))
							}
						}
					}
				} else {
					// If no alias found, just put the text in the list
					tmpDest = append(tmpDest, currentText)

				}
			}
			tmp = tmpDest
			//fmt.Printf("## %v", tmp)
			if !foundAlias {
				//fmt.Println("No alias found in this loop... finished!")
				finished = true
			}
		}
		result = tmp
		log.Debug(fmt.Sprintf("      %d items build from the original text due to aliases.", len(result)))
	} else {
		// If no alias found, just put the text in the list
		result = append(result, text)
	}

	return result
}

// processVariables()
// Search for variables, get their name, return the text with regexp groups for varibles
// The variables names will be extracted from the originalText
// The replacement will be done in the lowerText
// This is done this way because :
// 1. at this step, we process some lower case text
// 2. we still want to keep upper case in the variable names as they could be displayed to the user in some cases
// Returns : text, variables names
func processVariables(lowerText string, originalText string) (string, []string) {
	var vars []string
	var result string = ""
	if strings.Contains(originalText, "{{") {
		log.Debug("      {{ detected : there may be some variables.")

		finished := false
		for finished != true {
			if strings.Contains(originalText, "{{") && strings.Contains(originalText, "}}") {

				// search variable start
				openVarIdx := strings.Index(originalText, "{{")
				textBeforeVar := originalText[:openVarIdx]
				lowerTextBeforeVar := lowerText[:openVarIdx]

				// search var end
				tmpText := originalText[openVarIdx+2:]
				lowerTmpText := lowerText[openVarIdx+2:]

				closeVarIdx := strings.Index(tmpText, "}}")
				textAfterVar := tmpText[closeVarIdx+2:]
				lowerTextAfterVar := lowerTmpText[closeVarIdx+2:]
				varName := tmpText[:closeVarIdx]

				// remove useless \t and spaces
				varName = strings.TrimSpace(varName)

				// append the var name to the appropriate list
				vars = append(vars, varName)

				// replace the var in the text by the 'group regexp pattern'
				lowerText = fmt.Sprintf("%s(?P<%s>.*)%s", lowerTextBeforeVar, varName, lowerTextAfterVar)
				originalText = fmt.Sprintf("%s(?P<%s>.*)%s", textBeforeVar, varName, textAfterVar)
			} else {
				finished = true
			}

		}
		result = lowerText

	} else {
		// If no variables found, just return the text and no variables
		result = lowerText
	}
	return result, vars
}

// replaceStarsForRegexp()
// Replace the "*" by ".*" in the input text.
func replaceStarsForRegexp(text string) string {
	return strings.ReplaceAll(text, "*", ".*")

}

// getCompiledRegexp()
// Get the compiled regexp
func getCompiledRegexp(text string) *regexp.Regexp {
	// TODO : handle errors
	r, err := regexp.Compile(text)
	if err != nil {
		log.Error(fmt.Sprintf("Unable to compile the regular expression '%s'", text))
		return nil
	}
	return r
}

// displayLoadedInputRequests()
// This function is used mainly for debug.
// It displays in a nicely way all the loaded interactions.
func displayLoadedInputRequests(format string) {
	log.Debug("List of all text inputs :")
	if format == "verbose" {
		var previousId string
		for i := 0; i < len(textInputs); i++ {
			if previousId != textInputs[i].responseId {
				log.Debug(fmt.Sprintf("- Original text    : %s", textInputs[i].originalText))
				log.Debug(fmt.Sprintf("  Lang             : %s", textInputs[i].language))
				log.Debug(fmt.Sprintf("  Package          : %s", textInputs[i].pkg))
				log.Debug(fmt.Sprintf("  User priority    : %d", textInputs[i].userPriority))
				log.Debug(fmt.Sprintf("  Sys priority     : %d", textInputs[i].sysPriority))
				log.Debug(fmt.Sprintf("  Id               : %s", textInputs[i].responseId))
				log.Debug(fmt.Sprintf("  Variables        : %v", textInputs[i].variableNames))
			}
			log.Debug(fmt.Sprintf("  - Processed text   : %s", textInputs[i].text))
			previousId = textInputs[i].responseId
		}
	} else if format == "light" {
		var previousId string
		log.Debug(fmt.Sprintf("Sys prior. Lang.  User prior. Text"))
		for i := 0; i < len(textInputs); i++ {
			if previousId != textInputs[i].responseId {
				log.Debug(fmt.Sprintf("[%3d]      [%3s]  [%3d]       %s",
					textInputs[i].sysPriority,
					textInputs[i].language,
					textInputs[i].userPriority,
					textInputs[i].originalText))
			}
			previousId = textInputs[i].responseId
		}
	} else {
		log.Fatal(fmt.Sprintf("displayLoadedInputRequests(..) expects 'verbose' or 'light' as parameter. Value pased id '%s'", format))
	}
}

// displayLoadedResponses()
// This function is used mainly for debug.
// It displays in a nicely way all the loaded responses.
func displayLoadedResponses() {
	log.Debug("List of all responses :")
	for i := 0; i < len(responses); i++ {
		log.Debug(fmt.Sprintf("- Id            : %s", responses[i].responseId))
	}
}
