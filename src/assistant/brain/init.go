package brain

import (
	"assistant/logging"
	"fmt"
	"sort"
	"strings"
)

var Version string = "0.1"
var ConfigurationFile string = "brain-config.yml"
var defaultSysPriority uint16 = 50

var log logging.LoggingContext

// Notice : there is no init() function as the loading is initiated from 'something' : a REST/WebSocket server, a local client, ...
// The brain init() is done with the Load() function.

// Load()
// This function will start and load the brain configuration and data
func Load() {
	// Set logger
	log = logging.NewLogger("Brain", Version)
	log.Info("Start loading the brain...")

	// Load configuration
	C.getConfiguration()
	log.Debug(fmt.Sprintf("Configuration : %v", C))

	// List the packages files
	packagesFiles := packagesYamlFiles()
	log.Debug(fmt.Sprintf("Packages files : %v", packagesFiles))

	// TODO : check if we need to download the last releases of needed brain packages
	// TODO : if needed, download them

	// Load each package file
	//
	// You will notice that we read the same files several times... this is not a bug!
	// First we need to load some data from all files (aliases, ...).
	// Then we can load interactions.
	//
	// 1. load aliases
	for _, packageFile := range packagesFiles {
		loadPackageAliasesFromFile(packageFile.Language, packageFile.Package, packageFile.File)
	}
	// 2. load interactions
	for _, packageFile := range packagesFiles {
		loadPackageInteractionsFromFile(packageFile.Language, packageFile.Package, packageFile.File)
	}

	// Load internal stuff (default text response, ...)
	addAddtionnalRules()

	// Sort by language, priority
	sortTextInputRequests()

	// Some debugging display
	//displayLoadedInputRequests("light")
	displayLoadedInputRequests("verbose")
	displayLoadedResponses()

	log.Info("Brain loading finished :)")
}

// addAddtionnalRules()
// Add additionnal rules in the brain :
// - internal rules
// - ...
func addAddtionnalRules() {
	// TODO
}

// sortTextInputRequests()
// Sort the text input requests by :
// 1. system priority
// 2. favorite language
// 3. language by alphabetical order
// 4. user priority
func sortTextInputRequests() {
	favLang := AssistantFavoriteLanguage()
	// First, add "_" before the language if this is favorite language.
	// So this will automatically do the sort
	for k := 0; k < len(textInputs); k++ {
		if textInputs[k].language == favLang {
			textInputs[k].language = fmt.Sprintf("_%s", textInputs[k].language)
		}
	}

	log.Info(fmt.Sprintf("Sort input requests by : favorite language '%s' then language by alphabetical order, then priority", favLang))
	sort.Slice(textInputs, func(i, j int) bool {

		// Sort the "*" input entries to be the last ones
		if textInputs[i].sysPriority < textInputs[j].sysPriority {
			return false
		}
		if textInputs[i].sysPriority > textInputs[j].sysPriority {
			return true
		}

		// Sort languages by alphabetical order
		if textInputs[i].language < textInputs[j].language {
			return true
		}
		if textInputs[i].language > textInputs[j].language {
			return false
		}

		// then, the priority per user priority
		return textInputs[i].userPriority > textInputs[j].userPriority

	})

	// Finally remove the "_" before the favorite language
	for k := 0; k < len(textInputs); k++ {
		if strings.HasPrefix(textInputs[k].language, "_") {
			textInputs[k].language = strings.TrimPrefix(textInputs[k].language, "_")
		}
	}
}
