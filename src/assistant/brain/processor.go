package brain

import (
	"bytes"
	"fmt"
	"math/rand"
	"os/exec"
	"reflect"
	"regexp"
	"strings"
	"syscall"
	"time"
)

// Aliases

type Alias struct {
	language    string
	name        string
	replaceList []string
}

var aliases []Alias

// Interactions related to 'when_text' storage
// When a user sends a request to the brain, here is what happens :
//
//   The input request is read and in the Input structure we try to find a related entry.
//   If the entry is found, we get the related Response id. There may be an identical response id for several input requests.
//       Then, we search for the correct Response (text, actions) to apply in the Response structure and we apply it.
//   Else, we apply the B plan :)

type TextRequestInput struct { // note: later there may be other ****RequestInput
	originalText string         // the not process input text
	text         string         // the processed input text
	re           *regexp.Regexp // the processed input text
	userPriority uint16         // the priority defined by the user. From 0 to 255 : 0 is lower priority.
	// default priority : 100
	sysPriority   uint16   // the priority on technical side
	language      string   // the language of input text. This helps to filter/sort on the favorite language.
	pkg           string   // the language of input text. This helps to filter/sort on the favorite language.
	responseId    string   // the input id
	variableNames []string // the input id
}

type RequestResponse struct {
	responseId      string           // the input id : <language>_<package>_<number>
	language        string           // Language
	pkg             string           // package
	responseActions []ResponseAction // the list of actions to do
}

type ResponseAction struct {
	actionType string      // text, wait, ...
	parameters interface{} // anything
}

var textInputs []TextRequestInput
var responses []RequestResponse

// ProcessJsonExchangeFromWebsocket()
// This function will process a json input received by websockets
func ProcessJsonExchange(request BrainExchange, cbReceived func(BrainExchange, interface{}), conn interface{}) {
	// TODO : check request.Type value !

	// Copy some fields from the request
	// Set some values to the other ones
	response := BrainExchange{}
	response.Type = request.Type         // we will use the same type
	response.Media = request.Media       // we reply on the same media as the request
	response.ClientId = request.ClientId // we reply the same client id as the request as it may be used for internal routing
	response.Name = AssistantName()
	response.ConversationId = request.ConversationId // we stay in the same conversation (just obvious)
	response.RequestId = request.RequestId           // reponse is related to the request :)
	// TODO : response.ValidResponse = ???
	// TODO : response.ValidResponse = ???
	// TODO : response.ValidResponse = ???

	// Finally, get the reponse from the brain
	// TODO : later on, handle adding structured data, html cards, url, ...
	//response.Text = "pouet"
	ProcessRequest(request, cbReceived, conn, 0)

	//cbReceived(response, conn)
}

// ProcessRequest()
// Process a user Request
// Inputs :
// - the user request as BrainExchange
// - the function to call as func(BrainExchange, interface{})
//   The interface{} will be valued with cbResponseMedia
// - an optionnal (could be nil) 'thing' which could be needed by the callback function to send the response to the user
//   It could be a websocket connection or anything else. If nothing is needed, set it to nil
// - a security number about recursive calls (see redirect feature of the brain). The value should be set to 0
func ProcessRequest(request BrainExchange, cbResponse func(BrainExchange, interface{}), cbResponseMedia interface{}, numRecursiveCall uint16) {
	// TODO : replace the 'Error:' responses by error responses in the favorite language

	// TODO : check type = 'dialog'

	// TODO : set defaults value in request if fields are empty

	response := request
	text := request.Text

	// User name is a response is the assistant's one
	response.Name = AssistantName()
	// The message comes from the assistant
	response.FromAssistant = true
	// By default a response is valid
	response.ValidResponse = true
	// It this an ack message ? This will be processed later.
	response.Ack = false

	log.Info(fmt.Sprintf("ProcessRequest > input : %s", text))

	// Security for deepth and eternal loop
	if numRecursiveCall > 50 {
		response.Text = "Max recursive call reach... Stopping."
		processResponseError(response, cbResponse, cbResponseMedia)
		return
	}
	numRecursiveCall = numRecursiveCall + 1

	// Preprocessing
	log.Debug("Preprocess input...")
	log.Debug("- apply lowercase")
	newLower := strings.ToLower(text)

	// TODO : remove final . or !

	// Search compliant input
	log.Debug("Search for compliant input...")
	match := false
	var i int
	// Search a matching input
	log.Debug(fmt.Sprintf("Sys prior. Lang. User prior. Package      Id                                Text"))
	for i = 0; i < len(textInputs); i++ {
		log.Debug(fmt.Sprintf("[%3d]      [%3s] [%3d]       [%-10s] [%-30s]  %s",
			textInputs[i].sysPriority,
			textInputs[i].language,
			textInputs[i].userPriority,
			textInputs[i].pkg,
			textInputs[i].responseId,
			textInputs[i].text))
		if isMatching(newLower, textInputs[i].re) {
			match = true
			break
		}
	}
	if match {
		log.Debug("=> Match!")
		match = false // new usage: search response

		// OK, first check if the match is "*" or not.
		// If this is "*", it means "I don't understand" or something like that : the brain is not able to process the request.
		// So we will set the response as invalid.
		// The invalid status is important is a decentralized brain network :
		// if several brains are requested and only 1 can process the request, the client should :
		// - skip all invalid received responses, except the last one
		if textInputs[i].originalText == "*" {
			log.Debug("The brain is not able to understand the request. Set the response as invalid.")
			response.ValidResponse = false
		}

		// Get the variables values
		// TODO : explain
		varsMatch := textInputs[i].re.FindStringSubmatch(text)
		varsByName := make(map[string]string)
		for i, name := range textInputs[i].re.SubexpNames() {
			name = strings.TrimSpace(name) // remove useless \t or spaces
			if i != 0 && name != "" {
				varsByName[name] = varsMatch[i]
			}
		}
		log.Debug("Variables found : ")
		for k, v := range varsByName {
			log.Debug(fmt.Sprintf("- %s = %s", k, v))
			// Register variable in the cache
			SetVariable(k, v)
		}

		// Let's get the response Id!
		id := textInputs[i].responseId

		// Let's find the related response.
		var respIdx int
		for respIdx = 0; respIdx < len(responses); respIdx++ {
			log.Debug(fmt.Sprintf("[%s] [%s] id=%s", responses[respIdx].language,
				responses[respIdx].pkg,
				responses[respIdx].responseId))
			if id == responses[respIdx].responseId {
				log.Debug("=> Match!")
				match = true
				break
			}
		}

		if match {
			// Response found, process it
			// A response can be made of several actions. They will be executed sequentially.
			// But first, we will send an ACK to the client :
			// - this is usefull in decentralized system : even if the brain may took some time to respond,
			//   the client will known that the server is processing the request.

			// Process the ACK action
			// Notice that we send only the ack if the server is able to process the request!
			log.Debug("Execute a system action first : send an ACK to the client.")
			if response.ValidResponse == true {
				response.Ack = true
				response.Text = ""
				cbResponse(response, cbResponseMedia)
			}

			// Process the actions
			log.Debug(fmt.Sprintf("%+v", responses[respIdx].responseActions))
			response.Ack = false // the next actions are no more ACK

			// Process the actions (responses)
			handleActions(responses[respIdx].responseActions, response, varsByName, cbResponse, cbResponseMedia, numRecursiveCall)
		} else {
			// No response found... this is not normal!
			response.Text = fmt.Sprintf("No response found for id '%s'! This should not happen as there always should be a response for an input.", id)
			processInputError(response, cbResponse, cbResponseMedia)
			return
		}

	} else {
		// No match found... this is not normal!
		response.Text = "No match found! This should not happen as there always should be a default response."
		processInputError(response, cbResponse, cbResponseMedia)
		return
	}
}

// handleActions()
// - actionsList : the list of actions
// - response : the prebuilt response which will be completed
// Handle a list of actions
func handleActions(actionsList []ResponseAction, response BrainExchange, varsByName map[string]string, cbResponse func(BrainExchange, interface{}), cbResponseMedia interface{}, numRecursiveCall uint16) {
	for i := 0; i < len(actionsList); i++ {
		log.Debug(fmt.Sprintf("Executing response - part %d/%d", i+1, len(actionsList)))
		actionType := actionsList[i].actionType
		parameters := actionsList[i].parameters
		log.Debug(fmt.Sprintf("Action: %s", actionType))

		// Depending on the action type, do the appropriate thing

		switch actionType {
		case "text":
			// Return randomly one response in the available list

			// Check this is an array
			switch parameters.(type) {
			case []interface{}:
				// Randomly pick a response
				numAvailableResponses := len(parameters.([]interface{}))
				choice := rand.Intn(numAvailableResponses)
				response.Text = parameters.([]interface{})[choice].(string)

				// Complete response with variables
				// The response will be completed with all existing variables.
				var err error
				response.Text, err = ApplyVariablesToString(response.Text)
				if err != nil {
					processResponseError(response, cbResponse, cbResponseMedia)
					return
				}

				// call the callback to reply the response
				log.Info(fmt.Sprintf("ProcessRequest > response : %+v", response))
				cbResponse(response, cbResponseMedia)
			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A list of strings is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		case "wait":
			// Do a pause

			// Check is we have a map (dictionnary)
			switch reflect.ValueOf(parameters).Kind() {
			case reflect.Map:
				// the appropriate one, nothing to do
			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A dictionnary is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

			// Check parameter time_s type is int
			// TODO : the part below is not clear about type checking !!!!
			// TODO : review it
			switch parameters.(type) {
			case map[interface{}]interface{}:
				var timeSecond int
				timeSecond = parameters.(map[interface{}]interface{})["time_s"].(int)
				log.Info(fmt.Sprintf("ProcessRequest > wait %d seconds", timeSecond))
				log.Debug(fmt.Sprintf("Start a %d seconds pause", timeSecond))
				time.Sleep(time.Duration(timeSecond) * time.Second)
				log.Debug(fmt.Sprintf("End of the %d seconds pause", timeSecond))
			default:
				// In case of a string instead of int we would get this type : map[string]interface{}
				response.Text = fmt.Sprintf("Invalid value type for the action '%s', parameter '%s'. int required. found: '%s'", actionType, "time_s", reflect.TypeOf(parameters.(map[string]interface{})["time_s"]))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		case "redirect":
			// Redirect to another text input
			// Sample :
			//  - when_text:
			//      - "zz"
			//    do:
			//      - redirect: "ccc"

			// Check this is a string
			switch parameters.(type) {
			case string:
				redirect := parameters.(string)

				// Complete response with variables
				// The response will be completed with all existing variables.
				var err error
				redirect, err = ApplyVariablesToString(redirect)
				if err != nil {
					response.Text = redirect
					processResponseError(response, cbResponse, cbResponseMedia)
					return
				}

				// Recursive call
				log.Info(fmt.Sprintf("ProcessRequest > redirect to : %s", redirect))
				response.Text = redirect
				ProcessRequest(response, cbResponse, cbResponseMedia, numRecursiveCall)

			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A string is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		case "system":
			// Display all variables and their values
			// Sample :
			//  - when_text:
			//      - "system:vars"
			//    do:
			//      - system: vars

			// Check this is a string
			switch parameters.(type) {
			case string:
				sysaction := parameters.(string)

				log.Info(fmt.Sprintf("ProcessRequest > system action : %s", sysaction))
				switch sysaction {
				case "vars":
					response.Text = GetAllVariablesAsText()
				default:
					response.Text = fmt.Sprintf("Unknown system action '%s'", sysaction)
				}
				cbResponse(response, cbResponseMedia)

			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A string is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		case "execute_command":
			// Execute a comand line.
			// If the command line returns a 0 code return, execute the sub actions in 'do_if_success'
			// Else, execute the sub actions in 'do_if_error'
			// Sample :
			//  - when_text:
			//      - "exec"
			//  - execute_command:
			//      command: "echo 'tata' > /tmp/tata"
			//      do_if_success:
			//        - text: "success!"
			//        - text: "let's sleep!"
			//        - wait:
			//            time_s: 10
			//        - text: "let's wake up!"
			//      do_if_error:
			//        - text: "my bad, it fails!"

			// Check is we have a map (dictionnary)
			switch reflect.ValueOf(parameters).Kind() {
			case reflect.Map:
				// the appropriate one, nothing to do
			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A dictionnary is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

			switch parameters.(type) {
			case map[interface{}]interface{}:
				commandLine := parameters.(map[interface{}]interface{})["command"].(string)
				command0 := strings.Split(commandLine, " ")[0]
				command1n := strings.Join(strings.Split(commandLine, " ")[1:], " ")
				log.Info(fmt.Sprintf("Command to execute : '%s', '%s'", command0, command1n))
				// Execute the commande
				// We have a if because it seems that if command1n == "", it wil always fail...
				var cmd *exec.Cmd
				if command1n == "" {
					cmd = exec.Command(command0)
				} else {
					cmd = exec.Command(command0, command1n)
				}
				cmdOutput := &bytes.Buffer{}
				cmd.Stdout = cmdOutput
				cmd.Stderr = cmdOutput
				var waitStatus syscall.WaitStatus
				status := false
				if err := cmd.Run(); err != nil {
					// Error
					if err != nil {
						log.Info(fmt.Sprintf("Error whlie executing command: %s\n", err.Error()))
					}
					if exitError, ok := err.(*exec.ExitError); ok {
						waitStatus = exitError.Sys().(syscall.WaitStatus)
						log.Debug(fmt.Sprintf("Return code: %s", []byte(fmt.Sprintf("%d", waitStatus.ExitStatus()))))
						status = false
					}
				} else {
					// Success
					waitStatus = cmd.ProcessState.Sys().(syscall.WaitStatus)
					log.Debug(fmt.Sprintf("Return code: %s", []byte(fmt.Sprintf("%d", waitStatus.ExitStatus()))))
					log.Debug(fmt.Sprintf("Stdout : %s", string(cmdOutput.Bytes())))
					status = true
					// For each line of the output, check if it is in the variable format :
					//  varA=aaa
					//  varB=bbb
					// Warning ! If one line in the middle is not in the good format, the lines below
					// will be skipped... Example :
					//  var1=1
					//  var2=2
					//  some dummy line with no equal sign
					//  var3=this variable will never be set!

					for _, m := range strings.Split(string(cmdOutput.Bytes()), "\n") {
						log.Debug(m)
						tab := strings.Split(m, "=")
						if len(tab) >= 2 {
							// Valid format
							varName := strings.TrimSpace(tab[0])
							varValue := strings.TrimSpace(strings.Join(tab[1:], "=")) // rebuild the original value
							SetVariable(varName, varValue)
						} else {
							// Invalid format. We skip all the remaining lines.
							log.Info("No more variable in this output to process.")
							break
						}
					}
				}

				// Apply the rules after the command
				var cmdActions []ResponseAction
				if status == true {
					cmdActions = castInterfaceToSliceOfResponseAction(parameters.(map[interface{}]interface{})["do_if_success"])
				} else {
					cmdActions = castInterfaceToSliceOfResponseAction(parameters.(map[interface{}]interface{})["do_if_error"])
				}
				handleActions(cmdActions, response, varsByName, cbResponse, cbResponseMedia, numRecursiveCall)

			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A map is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		case "html":
			// Display some HTML content to the used in an iframe
			// Sample :
			//  - when_text:
			//      - "html"
			//    do:
			//      - html: >
			//          <h1>Title</h1>
			//          <p>foo bar</p>

			// Check this is a string
			switch parameters.(type) {
			case string:
				htmlContent := parameters.(string)

				log.Info(fmt.Sprintf("ProcessRequest > html content : '%s...'", htmlContent[0:15]))
				response.Text = ""
				response.Html = htmlContent
				cbResponse(response, cbResponseMedia)

			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A string is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		case "video":
			// Display some video to the user
			// Sample :
			//  - when_text:
			//      - "video"
			//    do:
			//      - video:
			//          url: "http://192.168.1.50/porche"
			//          format: "mjpeg"        # the video format : mjpeg, youtube, ... It will be used by the GUI to known how to display the video
			//          login: "user"          # some camera may be protected with basic authentification
			//          password: "password"   # some camera may be protected with basic authentification

			// Check is we have a map (dictionnary)
			switch reflect.ValueOf(parameters).Kind() {
			case reflect.Map:
				// the appropriate one, nothing to do
				log.Info(fmt.Sprintf("ProcessRequest > video content"))
			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A dictionnary is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

			switch parameters.(type) {
			case map[string]interface{}:
				name := parameters.(map[string]interface{})["name"].(string)
				url := parameters.(map[string]interface{})["url"].(string)
				format := parameters.(map[string]interface{})["format"].(string)
				// optionnales
				var login string
				tmpLogin := parameters.(map[string]interface{})["login"]
				if tmpLogin == nil {
					login = ""
				} else {
					login = tmpLogin.(string)
				}
				var password string
				tmpPassword := parameters.(map[string]interface{})["password"]
				if tmpPassword == nil {
					password = ""
				} else {
					password = tmpPassword.(string)
				}
				log.Debug(fmt.Sprintf("Video name = '%s'", name))
				log.Debug(fmt.Sprintf("Video url = '%s'", url))
				log.Debug(fmt.Sprintf("Video format = '%s'", format))
				log.Debug(fmt.Sprintf("Video login = '%s'", login))
				log.Debug(fmt.Sprintf("Video password = '%s'", password))

				response.Text = ""
				response.Video.Name = name
				response.Video.Url = url
				response.Video.Format = format
				response.Video.Login = login
				response.Video.Password = password
				cbResponse(response, cbResponseMedia)

			default:
				response.Text = fmt.Sprintf("Invalid value type for the action '%s'. A map is required. found: '%s'", actionType, reflect.TypeOf(parameters))
				processResponseError(response, cbResponse, cbResponseMedia)
				return
			}

		default:
			// Unknown action type...
			response.Text = fmt.Sprintf("Unknown action type '%s'. This should not happen!", actionType)
			processResponseError(response, cbResponse, cbResponseMedia)
			return
		}
	}

}

// ProcessError()
// Send a response error to the callback
// Input : the error message. No need to add "Error:" beside
func processResponseError(response BrainExchange, cbResponse func(BrainExchange, interface{}), cbResponseMedia interface{}) {
	// Complete message
	response.Text = fmt.Sprintf("Error while processing the response: %s", response.Text)
	response.ValidResponse = false
	// Log and send
	log.Error(response.Text)
	cbResponse(response, cbResponseMedia)
}

// ProcessError()
// Send a response error to the callback
// Input : the error message. No need to add "Error:" beside
func processInputError(response BrainExchange, cbResponse func(BrainExchange, interface{}), cbResponseMedia interface{}) {
	// Complete message
	response.Text = fmt.Sprintf("Error while processing the input: %s", response.Text)
	response.ValidResponse = false
	// Log and send
	log.Error(response.Text)
	cbResponse(response, cbResponseMedia)
}

// IsMatching()
// Check if the 'input' parameter matches the candidate
//func isMatching(input string, candidate string) bool {
func isMatching(input string, re *regexp.Regexp) bool {
	// TODO : improve :
	//        - substitusions
	//        - vars
	//        - ...
	log.Debug(fmt.Sprintf("IsMatching : '%s' vs re:'%v'", input, re))
	inputByte := []byte(input)
	matched := re.Match(inputByte)
	/*
		if input == candidate {
			return true
		}
		return false
	*/
	if matched {
		return true
	}
	return false
}
