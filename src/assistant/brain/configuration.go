package brain

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

/*
Sample configuration file :
  assistant:
    name: "butler"
    subname: "#1"       // the subname can be empty. It is used only for display on user interface in case there are several servers on the same host
    language: "fr"
  packages:
    folder: "./packages/"
    automatic_update: false
*/

// Configuration object
type configuration struct {
	Assistant configAssistant
	Packages  configPackages
}

type configAssistant struct {
	Name     string
	SubName  string
	Language string
}

type configPackages struct {
	Folder          string
	AutomaticUpdate bool
}

// Global variable to store the loaded configuration
var C configuration

// getConfiguration(...)
// Read the configuration file and parse it.
// The result will be stored in the C global variable

func (c *configuration) getConfiguration() *configuration {
	log.Info("Reading configuration...")
	yamlFile, err := ioutil.ReadFile(ConfigurationFile)
	if err != nil {
		log.Fatal(fmt.Sprintf("Error while opening configuration file : %v ", err))
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Fatal(fmt.Sprintf("Error while parsing YAML configuration file : %v", err))
	}

	log.Info("Configuration read with succes")

	return c
}

// AssistantName()
// Getter
func AssistantName() string {
	return C.Assistant.Name
}

// AssistantName()
// Getter
func AssistantTechnicalName() string {
	return fmt.Sprintf("%s %s", C.Assistant.Name, C.Assistant.SubName)
}

// AssistantFavoriteLanguage()
// Getter
func AssistantFavoriteLanguage() string {
	return C.Assistant.Language
}

// PackagesFolder()
// Getter
func PackagesFolder() string {
	return C.Packages.Folder
}
