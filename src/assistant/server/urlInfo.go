package main

import (
	"encoding/json"
	"net/http"
)

type Info struct {
	Component string `json:"component"`
	Version   string `json:"version"`
	Role      string `json:"role"`
	Name      string `json:"name"`
	Address   string `json:"address"`
}

// httpInfo()
// Return the /info json
func httpInfo(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	data := Info{Component, Version, Role, Name, Address}
	json.NewEncoder(w).Encode(data)
}
