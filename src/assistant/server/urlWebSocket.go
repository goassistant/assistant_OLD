package main

import (
	"assistant/brain"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"runtime"
)

// Upgrader for the websocket to upgrade from HTTP to TCP
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// wsBrainEndPoint()
// Endpoint to handle the ws connection
func wsBrainEndpoint(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	// upgrade this connection to a WebSocket
	// connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error(fmt.Sprintf("Websocket : %v", err))
	}

	log.Info("Websocket : New client connected")

	// Send some informations about the server
	systemMsg := brain.BrainExchange{
		Type:          "system",
		Os:            runtime.GOOS,
		Arch:          runtime.GOARCH,
		Hostname:      Hostname,
		AssistantName: brain.AssistantTechnicalName(),
	}
	err = ws.WriteJSON(systemMsg)
	if err != nil {
		log.Error(fmt.Sprintf("Websocket : %v", err))

	}
	// listen indefinitely for new messages coming
	// through on our WebSocket connection
	wsReader(ws)
}

// wsReader()
// Listen eternally on websocket to read data on it
func wsReader(conn *websocket.Conn) {
	for {
		// read in a json message
		// messageType is : websocket.BinaryMessage or websocket.TextMessage (1)
		var request brain.BrainExchange

		// Debug - In case of issue when receiving something that is not json, enable these lines
		//mt, m, e := conn.ReadMessage()
		//fmt.Println(mt)
		//fmt.Printf("%s", string(m))
		//fmt.Println(e)
		// Debug - End

		err := conn.ReadJSON(&request)
		if err != nil {
			// if error, it could be because of :
			// - a closed connection
			// - some non json data transmitted : it seems to finish in a closed connection
			log.Error(fmt.Sprintf("Websocket > received : %v", err))
			return
		}

		// handle messages
		log.Debug(fmt.Sprintf("Websocket > received : "))
		log.PrettyPrint(request)

		// Call the brain
		go brain.ProcessJsonExchange(request, handleBrainResponse, *conn)

	}
}

//func handleBrainResponse(response brain.BrainExchange, conn *websocket.Conn) {
func handleBrainResponse(response brain.BrainExchange, conni interface{}) {
	conn := conni.(websocket.Conn)
	log.Debug(fmt.Sprintf("Websocket > writing : "))
	log.PrettyPrint(response)

	// Send the response
	err := conn.WriteJSON(response)
	if err != nil {
		log.Error(fmt.Sprintf("Websocket > writing : %v", err))
		return
	}
}
