package logging

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

// Some resources :
// https://stackoverflow.com/questions/18361750/correct-approach-to-global-logging-in-golang
// https://www.reddit.com/r/golang/comments/5ovkut/sharing_loggers_across_contexts_and_packages/
// context : https://blog.gopheracademy.com/advent-2016/context-logging/

// The logging context
type LoggingContext struct {
	Application      string
	Component        string
	ComponentVersion string
	DiscussionId     string
	HostId           string
	HostOSArch       string // OS and Architecture : windows-amd64 for example
}

// init()
// Function called when the package is imported
func init() {
	profile := os.Getenv("LOG_OUTPUT")
	// Values :
	// - TTY : on the console
	// - FILE : in a file
	// - anything else or empty : file
	if profile == "TTY" {
		// Console output

		// TODO : allow to configure loglevel
		log.SetLevel(log.DebugLevel)
		log.SetFormatter(&log.TextFormatter{
			DisableColors:   false,
			FullTimestamp:   true,
			TimestampFormat: "2006-01-02T15:04:05.000",
		})

	} else {
		// File output
		var logFile = "assistant.log"
		envLogFile := os.Getenv("LOG_FILE")
		if envLogFile != "" {
			logFile = envLogFile
		}

		// TODO : allow to configure loglevel
		log.SetLevel(log.DebugLevel)
		log.SetFormatter(&log.JSONFormatter{})

		// You could set this to any `io.Writer` such as a file
		file, err := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err == nil {
			//log.Out = file
			log.SetOutput(file)
		} else {
			log.Error("Failed to log to file, using default stderr")
		}
		log.Info("Coucou")
	}
}

// NewLogger()
// Function called to init the logger in a package
// It returns the log context on which the Info(), ... functions are called.
func NewLogger(component string, version string) LoggingContext {
	app := filepath.Base(os.Args[0])
	var ctx = LoggingContext{}
	ctx.Application = app
	ctx.Component = component
	ctx.ComponentVersion = version
	ctx.HostOSArch = fmt.Sprintf("%s-%s", runtime.GOOS, runtime.GOARCH)
	ctx.HostId = "n/a" // TODO : find a way to populate it without package machineid which needs some CGO dependencies!
	return ctx
}

// Debug()
func (ctx LoggingContext) Debug(text string) {
	// fmt.Println(" DEBUG[....] " + text) // TODO : remove when debug level will be enabled
	log.WithFields(log.Fields{
		"application":       ctx.Application,
		"component":         ctx.Component,
		"component_version": ctx.ComponentVersion,
		"discussion_id":     ctx.DiscussionId,
		"host_os_arch":      ctx.HostOSArch,
		"host_id":           ctx.HostId,
	}).Debug(text)
}

// Info()
func (ctx LoggingContext) Info(text string) {
	log.WithFields(log.Fields{
		"application":       ctx.Application,
		"component":         ctx.Component,
		"component_version": ctx.ComponentVersion,
		"discussion_id":     ctx.DiscussionId,
		"host_os_arch":      ctx.HostOSArch,
		"host_id":           ctx.HostId,
	}).Info(text)
}

// Warn()
func (ctx LoggingContext) Warn(text string) {
	// TODO : add a handler at log init (optionnal)
	//        If the handler is defined, call it with the log message.
	//        The handler my be use to display error messages in real time on user interface (web, android, whatever)
	log.WithFields(log.Fields{
		"application":       ctx.Application,
		"component":         ctx.Component,
		"component_version": ctx.ComponentVersion,
		"discussion_id":     ctx.DiscussionId,
		"host_os_arch":      ctx.HostOSArch,
		"host_id":           ctx.HostId,
	}).Warn(text)
}

// Error()
func (ctx LoggingContext) Error(text string) {
	// TODO : add a handler at log init (optionnal)
	//        If the handler is defined, call it with the log message.
	//        The handler my be use to display error messages in real time on user interface (web, android, whatever)
	log.WithFields(log.Fields{
		"application":       ctx.Application,
		"component":         ctx.Component,
		"component_version": ctx.ComponentVersion,
		"discussion_id":     ctx.DiscussionId,
		"host_os_arch":      ctx.HostOSArch,
		"host_id":           ctx.HostId,
	}).Error(text)
}

// Fatal()
// Log and exit
func (ctx LoggingContext) Fatal(text string) {
	log.WithFields(log.Fields{
		"application":       ctx.Application,
		"component":         ctx.Component,
		"component_version": ctx.ComponentVersion,
		"discussion_id":     ctx.DiscussionId,
		"host_os_arch":      ctx.HostOSArch,
		"host_id":           ctx.HostId,
	}).Fatal(text)
}

//
// Pretty printing functions...
// These functions are only used for debugging. And so, they use log.Debug at the end.
//

// PrettyPrint()
func (ctx LoggingContext) PrettyPrint(i interface{}) {
	s, _ := json.MarshalIndent(i, "", "  ")
	ctx.Debug("\n" + string(s))
}

//
// HTTP servers related functions...
//

// logRequest()
// Wrapper to call the url and log informations on the call
func (ctx LoggingContext) HttpRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx.Debug(fmt.Sprintf("Url call : %s %s %s", r.RemoteAddr, r.Method, r.URL))

		// ctx.Debug("Processing headers...")
		// for name, headers := range r.Header {
		// 	name = strings.ToLower(name)
		// 	for _, h := range headers {
		// 		ctx.Debug(fmt.Sprintf("header > %v: %v", name, h))
		// 	}
		// }

		// Add CORS
		// TODO : function in security/auth still needed with this ??
		// TODO : function in security/auth still needed with this ??
		// TODO : function in security/auth still needed with this ??
		(w).Header().Set("Access-Control-Allow-Origin", "*")
		(w).Header().Set("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH")
		(w).Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")

		if r.Method == "OPTIONS" {
			// Handle the preflight request
			// - we will always respond HTTP 200 with "Preflight allowed"
			// - no check on user credentials as this is only a preflight request
			ctx.Debug("Preflight request received. Responding HTTP 200")
			w.Write([]byte("allowed"))
			return
		}

		// Now the log part
		start := time.Now()     // 1 second = 1 000 000 000 (nano seconds!!)
		handler.ServeHTTP(w, r) // Call url function
		t := time.Now()
		elapsed := t.Sub(start) / 1000000 // to get milli seconds
		ctx.Debug(fmt.Sprintf("Url response : %s %s %s", r.RemoteAddr, r.Method, r.URL))

		// TODO : log HTTP status code
		ctx.Info(fmt.Sprintf("Performance: time_ms=%d, method=%s, url=%s, remote_addr=%s", elapsed, r.Method, r.URL, r.RemoteAddr))
	})
}
