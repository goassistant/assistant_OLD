# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from a minimal image
FROM bitnami/minideb:latest

# Add Maintainer Info
LABEL maintainer="Fritz <fritz.smh@gmail.com>" \
      project="{LABEL}"

# Set the Current Working Directory inside the container
WORKDIR /app/{APPNAME}

# Add the directory to the image
ADD {DIR} ./

# Create a log folder
# This folder can be used to store the log file with the environment variable LOG_FILE. 
# Then with a volume the log folder can be shared between containers.
RUN mkdir -p /log/

# Expose port to the outside world
{EXPOSE_PORT}

# Command to run the executable
CMD ["/app/{APPNAME}/{BINARY}"]

